def part_one(numbers):
    dx = 1E6
    for i in range(max(numbers)):
        dxi = sum([abs(num - i) for num in numbers])
        if dxi < dx: dx = dxi
    return dx

def part_two(numbers):
    # This can be refactored into a one-liner l-comprehensiion
    # at the cost of readability and no real speed gain
    dx = 1E16
    for i in range(max(numbers)):
        # Apply summation \sum_i=0^n{n} = n/2(1+n)
        dxi = sum([abs(num - i)*(1 + abs(num - i)) for num in numbers])
        if dxi < dx: dx = dxi
    # Recover division /2 from equation
    return dx/2

if __name__ == '__main__':
    numbers = [int(num) for num in 
               open('../data/seven.txt').readline().split(',')]

    result = part_one(numbers)
    print('Part 1:', result)

    result = part_two(numbers)
    print('Part 2:', result)