def part_one(numbers):
    # Count increasing values
    n = len(numbers)
    return sum(1 for i in range(n - 1) 
               if numbers[i + 1] > numbers[i])
    
def part_two(numbers):
    # Count increasing sums of groups of three a+b+c < b+c+d = a < d
    n = len(numbers)
    return sum(1 for i in range(n - 2) 
               if numbers[i + 2] > numbers[i])

if __name__ == '__main__':
    test_data = [199, 200, 208, 210, 200, 207, 240, 269, 260, 263]

    with open('../data/one.txt') as f:
        numbers = [int(val) for val in f]
    
    result = part_one(numbers)
    print('Part 1:', result)

    result = part_two(numbers)
    print('Part 2:', result)