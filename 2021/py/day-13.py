def day_thirteen(coords, steps):
    for axis, val in steps:
        if axis == 'x':
            to_remove = {(x, y) for x, y in coords if x >= val}
            to_add = {(x - 2*(x - val), y) for x, y in coords if x >= val}
        if axis == 'y':
            to_remove = {(x, y) for x, y in coords if y >= val}
            to_add = {(x, y - 2*(y - val)) for x, y in coords if y >= val}
        coords.difference_update(to_remove)
        coords.update(to_add)
    return len(coords)

def print_paper(coords):
    maxx = max([coord[0] for coord in coords]) + 1
    maxy = max([coord[1] for coord in coords]) + 1
    output = [['.' for i in range(maxx)] for j in range(maxy)]

    for x, y in coords:
        output[y][x] = '#'

    for line in output:
        print(line)

if __name__ == '__main__':
    with open('../data/thirteen.txt') as f:
        data = []
        steps = []
        for line in f:
            if not line.strip(): continue
            if 'fold' in line:
                steps.append(line.strip().split()[-1].split('='))
            else:
                data.append(tuple(map(int, line.strip().split(','))))
        steps = [(axis, int(val)) for axis, val in steps]

    result = day_thirteen(set(data), steps)
    print('Part 1 & 2', result)