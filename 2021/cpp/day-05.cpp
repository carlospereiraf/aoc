#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <string>

using namespace std;

int main(int argc, char const *argv[])
{
  vector<vector<int>> boards;
  vector<int> board,nums,winners,*boardp;
  string line;
  bool read_nums = false;
  ifstream fid("../data/four.txt");
  int result1,result2,m;

  int nb = 0;
  while(getline(fid,line))
  {
    if(line.empty()) 
    {
      board.clear();
      continue;
    }
    stringstream ss(line);
    string value;
    if(!read_nums)
    {
      // Read up drawn numbers
      while(getline(ss,value,'->')) 
          {
            nums.push_back(stoi(value));
          }

      x1 << va;ie
      read_nums = true;
    }
    else
    {
      while(ss >> value) board.push_back(stoi(value));
      if(board.size()==25)
      { 
        boards.push_back(board);
        nb++;
      }
    }
  }
  for(auto num: nums)
  {
    for(int i=0; i<nb; i++)
    {
      boardp = &boards[i];
      if(iswinner(winners,i)) continue;
      replace(boardp->begin(),boardp->end(),num,-1);
      if(bingo(*boardp))
      {
        if(winners.size()==0) result1 = sum_unmarked(boardp)*num;
        if(winners.size()==nb-1) result2 = sum_unmarked(boardp)*num;
        winners.push_back(i);
      }
    }
  }

  printf("Part 1: %d\n", result1);
  printf("Part 2: %d\n", result2);
  return 0;
}

