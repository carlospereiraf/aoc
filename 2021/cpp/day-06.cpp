// #include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <string>

using namespace std;

int main(int argc, char const *argv[])
{
  vector<int> nums;
  long fish[9],spawn,sum;
  int ndays;
  string line,value;

  ndays = 256;

  for(int i=0; i<9; i++) fish[i] = 0;

  // Read input data
  ifstream fid("../data/six.txt");
  getline(fid,line);
  stringstream ss(line);
  while(getline(ss,value,',')) nums.push_back(stoi(value));

  // Add initial counts
  for(auto j: nums) fish[j] += 1;

  // Update daily counts
  for(int i=0; i<ndays; i++)
  {
    spawn = fish[0];
    for(int j=0; j<8; j++) fish[j] = fish[j+1];
    fish[6] += spawn;
    fish[8] = spawn;
  }

  sum = 0;
  for(int i=0; i<9; i++)  sum += fish[i];
  printf("Part 1 & 2: %ld\n", sum);
  return 0;
}

