#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <string>

using namespace std;

int main(int argc, char const *argv[])
{
  vector<int> nums;
  long dxmin,dxcurr;
  int maxdist;
  string line,value;

  // Read input data
  ifstream fid("../data/seven.txt");
  getline(fid,line);
  stringstream ss(line);
  while(getline(ss,value,',')) nums.push_back(stoi(value));

  // Retrieve maximum value
  maxdist = 0;
  for(auto num: nums)
  {
    if(num > maxdist) maxdist = num;
  }

  // Part one
  dxmin = INT_MAX;
  for(int i=0; i<maxdist; i++)
  {
    dxcurr = 0;
    for(auto num: nums) dxcurr += abs(num-i);
    if(dxcurr<dxmin) dxmin = dxcurr;
  }
  printf("Part 1: %ld\n", dxmin);

  // Part two (Apply \sum_i=0^n{n} = n/2(1+n))
  dxmin = INT_MAX;
  for(int i=0; i<maxdist; i++)
  {
    dxcurr = 0;
    for(auto num: nums) dxcurr += abs(num-i)*(1+abs(num-i));
    if(dxcurr<dxmin) dxmin = dxcurr;
  }
  printf("Part 2: %ld\n", dxmin/2);
  return 0;
}

